package edu.sjsu.android.threadeddownloadactivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import edu.sjsu.android.threadeddownloadactivity.R;


public class MainActivity extends Activity {

    ProgressDialog dialog;
    int progressStep = 20;
    final int MAX_PROGRESS = 100;
    int globalVar = 0;
    int accum = 0;
    long startingMills = System.currentTimeMillis();
    boolean isRunning = false;
    Handler myHandler = new Handler();
    ProgressDialog dialog2;
    protected boolean isRunning2 = false;
    protected final int MAX_SEC = 30;
    Long startingMillis;

    TextView textView1;
    EditText editText1;
    ImageView imageView1;

    Button buttonRunnable;
    Button buttonMessages;
    Button buttonAsync;
    Button buttonReset;

    Bitmap myBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView1 = (TextView) findViewById(R.id.TextView);

        editText1 = (EditText) findViewById(R.id.EditText);
        editText1.setHint("Enter URL");
        editText1.setText("");

        imageView1 = (ImageView) findViewById(R.id.imageView);
        imageView1.setImageResource(R.drawable.apple);
        buttonRunnable = (Button) findViewById(R.id.button);
        buttonMessages = (Button) findViewById(R.id.button2);
        buttonAsync = (Button) findViewById(R.id.button3);
        buttonReset = (Button) findViewById(R.id.button4);

        dialog = new ProgressDialog(MainActivity.this);
        dialog2 = new ProgressDialog(MainActivity.this);

        dialog2.setProgress(0);
        dialog2.setMax(MAX_SEC);

        buttonRunnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                methodRunnable();
            }
        });

        buttonMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                methodMessages();
            }
        });

        this.buttonAsync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                new VerySlowTask().execute("dummy1", "dummy2");
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText1.setHint("Enter URL");
                editText1.setText("");
                imageView1.setImageResource(R.drawable.apple);            }
        });


    }

    void downloadBitmap() throws IOException {
        String url = editText1.getText().toString();
        URL url2 = new URL(url);
        HttpURLConnection urlC = (HttpURLConnection) url2.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlC.getInputStream());
            myBitmap = BitmapFactory.decodeStream(in);
        } finally {
            urlC.disconnect();
        }

    }


    protected void methodRunnable() {
        super.onStart();

        accum = 0;
        dialog.setMessage("Downloading by Handler & Runnable");
        dialog.setMax(MAX_PROGRESS);
        dialog.setProgress(0);

        // create-start background thread were the busy work will be done
        Thread myBackgroundThread = new Thread(backgroundTask, "backAlias1");
        myBackgroundThread.start();

    }

    private Runnable foregroundRunnable = new Runnable() {
        @Override

        public void run() {

            try {
                dialog.incrementProgressBy(progressStep);
                accum += progressStep;
                dialog.show();

                if (accum >= dialog.getMax()) {

                    dialog.dismiss();
                    imageView1.setImageBitmap(myBitmap);
                }

            }
            catch (Exception e) {
                Log.e("<<foregroundTask>>", e.getMessage());
            }
        }
    };

    private Runnable backgroundTask = new Runnable() {
        public void run() {
            try {
                downloadBitmap();
                for (int n = 0; n < 10; n++) {
                    Thread.sleep(2000);
                    globalVar++;
                    myHandler.post(foregroundRunnable);

                }
            }
            catch (InterruptedException | IOException e) {
                Log.e("<<foregroundTask>>", e.getMessage());
            }
        }
    };


    Bitmap downloadBitmap2(String url) {
        Bitmap image = null;
        try {
            InputStream input = (InputStream) new URL(url).getContent();
            image = BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            image = null;
            Log.d(getClass().getSimpleName(), "Failure in downloading Bitmap.");
            myHandler2.post(new Runnable() {
                public void run() {
                    (Toast.makeText(getApplicationContext(), "Failure in downloading Bitmap.", Toast.LENGTH_LONG)).show();
                }
            });
        }
        return image;
    }

    Handler myHandler2 = new Handler() {
        public void handleMessage(Message msg) {
            Bitmap returnedValue = (Bitmap) msg.obj;
            imageView1.setImageBitmap(returnedValue);
            dialog2.dismiss();
        }
    };

    public void methodMessages() {
        super.onStart();

        dialog2.setMessage("Downloading by Handler & Messages");
        dialog2.incrementProgressBy(5);
        dialog2.show();
        if (dialog2.getProgress() == MAX_SEC) {
            isRunning2 = false;
        }

        if (dialog2.getProgress() == dialog2.getMax()) {

        }

        else {

        }

        Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    for (int i = 0; i < MAX_SEC && isRunning2; i++) {
                        Thread.sleep(5000);
                        Bitmap data2 = downloadBitmap2(editText1.getText().toString());
                        Message msg = myHandler2.obtainMessage(1, (Bitmap)data2);

                        if (isRunning2) {
                            myHandler2.sendMessage(msg);
                        }
                    }
                }
                catch (Exception e) {
                    isRunning2 = false;
                    e.printStackTrace();
                }

            }
        });

        isRunning2 = true;
        background.start();
    }

    public void onStop() {
        super.onStop();
        isRunning2 = false;
    } // onStop

    private class VerySlowTask extends AsyncTask<String, Long, Void> {
        private final ProgressDialog dialog3 = new ProgressDialog(MainActivity.this);
        String waitMsg = "Wait For Download by AsyncTask\nSome SLOW job is being done...";

        protected void onPreExecute() {
            startingMillis = System.currentTimeMillis();
            this.dialog3.setMessage(waitMsg);
            this.dialog3.setCancelable(false);
            this.dialog3.show();
        }

        @Override
        protected Void doInBackground(final String... args) {
            Log.e("doInBackground>>", "Total args: " + args.length);
            Log.e("doInBackground>>", "args[0] = " + args[0]);

            try {
                downloadBitmap();
                for (Long i = 0L; i < 5L; i++) {
                    Thread.sleep(5000);
                    publishProgress((Long) i);

                }

            }
            catch (InterruptedException | IOException e) {
                Log.e("slow-job interrupted", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Long... value) {
            super.onProgressUpdate(value);
            dialog3.setMessage(waitMsg + value[0]);
        }

        @Override
        protected void onPostExecute(final Void unused) {
            if (this.dialog3.isShowing()) {
                this.dialog3.dismiss();
            }

            imageView1.setImageBitmap(myBitmap);
        }
    }

}